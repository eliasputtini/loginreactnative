![enter image description here](https://miro.medium.com/max/1400/1*xDi2csEAWxu95IEkaNdFUQ.png)


#### [RocketSeat.com.br](https://rocketseat.com.br/)

Video do tutorial:

#### [Autenticação no React Native / ReactJS com Context API & Hooks | Masterclass #12](https://www.youtube.com/watch?v=KISMYYXSIX8&feature=youtu.be)

## Descrição:

A aplicação faz login simulando a utilização de uma api, salvando os dados com AsyncStorage e mantendo o usuário logado.


## Prerequisitos


Utilize algum instalador de pacotes como yarn ou npm para escrever os comandos a seguir no cmd


## Instalação


### Mobile 

Abra a pasta `./authrn/`  no cmd e digite: 


```
yarn install
```

Rode a aplicação no emulador/dispositivo dependendo do sistema:
```
yarn android 
```
Iphone/Mac
```
yarn ios
```



Foram usados neste projeto as tecnologias de React Native / ReactJS com Context API & Hooks, AsyncStorage e TypeScript